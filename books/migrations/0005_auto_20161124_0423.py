# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import books.validators


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0004_book_cover'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='cover',
            field=models.ImageField(upload_to='covers', validators=[books.validators.validate_file_extension]),
        ),
    ]
