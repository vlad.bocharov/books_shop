# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0019_order_user_cookie'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='pub_date',
            field=models.DateTimeField(verbose_name='date published', default=datetime.datetime(2016, 12, 6, 4, 7, 7, 187678, tzinfo=utc)),
        ),
    ]
