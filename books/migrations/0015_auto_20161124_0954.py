# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import books.validators


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0014_auto_20161124_0954'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='cover',
            field=models.ImageField(upload_to='images', height_field='100px', validators=[books.validators.validate_file_extension]),
        ),
    ]
