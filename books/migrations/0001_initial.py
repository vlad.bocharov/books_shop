# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Books',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('author', models.CharField(max_length=40)),
                ('year_publishing', models.IntegerField()),
                ('themes', models.CharField(max_length=20)),
                ('category', models.CharField(max_length=10)),
                ('price', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='BooksOrders',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('count', models.IntegerField(default=1)),
                ('book', models.ForeignKey(to='books.Books')),
            ],
        ),
        migrations.CreateModel(
            name='Orders',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('pub_date', models.DateTimeField(verbose_name='date published')),
                ('customer', models.CharField(max_length=40)),
                ('delivery_address', models.CharField(max_length=60)),
                ('status', models.CharField(max_length=10)),
            ],
        ),
        migrations.AddField(
            model_name='booksorders',
            name='order',
            field=models.ForeignKey(to='books.Orders'),
        ),
    ]
