# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0018_book_desc'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='user_cookie',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
    ]
