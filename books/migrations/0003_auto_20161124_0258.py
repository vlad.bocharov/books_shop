# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0002_books_title'),
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('author', models.CharField(max_length=40)),
                ('year_publishing', models.IntegerField()),
                ('themes', models.CharField(max_length=20)),
                ('title', models.CharField(max_length=40)),
                ('category', models.CharField(max_length=10)),
                ('price', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='BooksOrder',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('count', models.IntegerField(default=1)),
                ('book', models.ForeignKey(to='books.Book')),
            ],
        ),
        migrations.RenameModel(
            old_name='Orders',
            new_name='Order',
        ),
        migrations.RemoveField(
            model_name='booksorders',
            name='book',
        ),
        migrations.RemoveField(
            model_name='booksorders',
            name='order',
        ),
        migrations.DeleteModel(
            name='Books',
        ),
        migrations.DeleteModel(
            name='BooksOrders',
        ),
        migrations.AddField(
            model_name='booksorder',
            name='order',
            field=models.ForeignKey(to='books.Order'),
        ),
    ]
