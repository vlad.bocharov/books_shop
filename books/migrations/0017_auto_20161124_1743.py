# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0016_auto_20161124_1332'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='category',
            field=models.IntegerField(default=0),
        ),
    ]
