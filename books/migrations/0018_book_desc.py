# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0017_auto_20161124_1743'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='desc',
            field=models.CharField(max_length=200, default='Good staff'),
            preserve_default=False,
        ),
    ]
