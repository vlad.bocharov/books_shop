# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import books.validators


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0007_auto_20161124_0439'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='cover',
            field=models.ImageField(validators=[books.validators.validate_file_extension], upload_to='images'),
        ),
    ]
