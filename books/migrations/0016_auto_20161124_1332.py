# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import books.validators


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0015_auto_20161124_0954'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='cover',
            field=models.ImageField(validators=[books.validators.validate_file_extension], upload_to='images'),
        ),
        migrations.AlterField(
            model_name='book',
            name='themes',
            field=models.IntegerField(default=1),
        ),
    ]
