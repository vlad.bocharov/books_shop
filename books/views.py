import time
from django.http import HttpResponse, Http404, HttpResponseRedirect, JsonResponse
from django.utils import timezone
from django.db.models import Q
# from django.template import loader
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from rest_framework import viewsets
import uuid

from django.contrib.auth.models import User, Group
from .models import Book, Order, BooksOrder
from .forms import OrderForm
from .serializers import UserSerializer, GroupSerializer, BookSerializer

def get_order_count(request):
    if 'order' in request.session:
        order = get_object_or_404(Order, user_cookie=request.session['order'])
        order_count = order.books_count()
    else:
        order_count = 0
    return order_count

def index(request):
    if request.method == 'GET':
        books = Book.objects.order_by('-pk')
        context = {
            'books': books,
            'order_count': get_order_count(request)
        }
        print('context', context)
        return render(request, 'books/index.html', context)
    elif request.method == 'POST':
        kargs = []
        kwargs = {}
        if 'category_book' not in request.POST and 'category_magazine' not in request.POST:
            return render(request, 'books/index.html', {'books': None})
        elif 'category_book' not in request.POST and 'category_magazine' in request.POST:
            kwargs['category'] = 1
        elif 'category_book' in request.POST and 'category_magazine' not in request.POST:
            kwargs['category'] = 0
        kwargs['year_publishing__gte'] = request.POST['year_from']
        kwargs['year_publishing__lte'] = request.POST['year_to']
        if int(request.POST['themes']) > 0:
            kwargs['themes'] = request.POST['themes']
        kargs.append(Q(title__icontains=request.POST['title']))
        kargs.append(Q(author__icontains=request.POST['author']))
        books = Book.objects.filter(*kargs, **kwargs)
        print(books)
        return render(request, 'books/index.html', {'books': books, 'order_count': get_order_count(request)})

def detail(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    return render(request, 'books/detail.html', {'book': book, 'order_count': get_order_count(request)})

def order(request, book_id):
    book = get_object_or_404(Book, id=book_id)
    print(book)
    # get or create order
    user_cookie = request.session.get('order', False)
    if not user_cookie:
        print('not order')
        user_cookie = request.session['order'] = str(uuid.uuid4())
    print('user_cookie', request.session['order'])
    order = Order.objects.filter(user_cookie=user_cookie).first()
    if not order:
        order = Order(pub_date=timezone.now(), user_cookie=request.session['order'], status=0)
        order.save()
    print(order)
    print(order.id)
    # get or update BooksOrder
    books_order = BooksOrder.objects.filter(book=book, order=order).first()
    if not books_order:
        print('new book_order')
        books_order = BooksOrder.objects.create(book_id=book.id, order_id=order.id, count=1)
        books_order.save()
    else:
        print('update book_order')
        books_order.count += 1
        books_order.save()
    print(books_order)
    return JsonResponse({'status': 'success', 'order_count': order.books_count()})


def order_page(request):
    order = get_object_or_404(Order, user_cookie=request.session['order'])
    print(order)
    print(order.customer)
    books = order.books()
    if request.method == 'POST':
        formset = OrderForm(request.POST, instance=order)
        if formset.is_valid():
            formset.save()
            # do something.
            return render(request, 'books/index.html')
    else:
        formset = OrderForm()
    return render(request, 'books/order.html', {'order': order, 'books': books, 'formset': formset, 'order_count': get_order_count(request)})


def test_cache(request):
    time.sleep(3)
    return render(request, 'books/bum.html')


# API
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class BookViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows books to be viewed or edited.
    """
    queryset = Book.objects.all()
    serializer_class = BookSerializer