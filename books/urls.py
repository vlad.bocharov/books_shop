from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<book_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^(?P<book_id>[0-9]+)/order/$', views.order, name='order'),
    url(r'^order/$', views.order_page, name='order_page'),
    url(r'^wait/$', views.test_cache, name='test_cache'),
]
