from django.utils import timezone
from django.db import models
from .validators import validate_file_extension

class Book(models.Model):
	author = models.CharField(max_length=40)
	year_publishing = models.IntegerField()
	# 1 - Study, 2 - Techniq, 3 - Detective; hardcoded now
	themes = models.IntegerField(default=1)
	title = models.CharField(max_length=40)
	cover = models.ImageField(upload_to='images', validators=[validate_file_extension])
	# 0 - books, 1 - magazine
	category = models.IntegerField(default=0)
	desc = models.CharField(max_length=200)
	price = models.FloatField()

	def __str__(self):
		return self.title


class Order(models.Model):
	pub_date = models.DateTimeField('date published', default=timezone.now())
	user_cookie = models.CharField(max_length=100)
	customer = models.CharField(max_length=40)
	delivery_address = models.CharField(max_length=60)
	# new, pending, delivered, canceled, return
	status = models.IntegerField(default=0)

	def __str__(self):
		return self.user_cookie

	def books_count(self):
		books_orders=BooksOrder.objects.filter(order_id=self.id).all()
		res = 0
		for bo in books_orders:
			res += bo.count
		return res

	def books(self):
		books_orders=BooksOrder.objects.filter(order_id=self.id).all()
		res = []
		for bo in books_orders:
			res.append({'book': bo.book, 'count': bo.count})
		return res


class BooksOrder(models.Model):
	book = models.ForeignKey(Book)
	order = models.ForeignKey(Order)
	count = models.IntegerField(default=1)

	def __str__(self):
		return 'Book {}; Order: {}; Count: {}'.format(self.book, self.order, self.count)
