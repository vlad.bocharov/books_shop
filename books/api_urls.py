from django.conf.urls import url, include
from rest_framework import routers
from .views import UserViewSet, GroupViewSet, BookViewSet

router = routers.SimpleRouter()
router.register(r'users', UserViewSet)
router.register(r'group', GroupViewSet)
router.register(r'books', BookViewSet)

urlpatterns = router.urls