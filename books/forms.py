from django.forms import CharField, ModelForm
from .models import Order

class OrderForm(ModelForm):

	class Meta:
		model = Order
		fields = ('customer', 'delivery_address')
	    # customer = forms.CharField(max_length=40, required=True)
	    # delivery_address = forms.CharField(max_length=60, required=True)
